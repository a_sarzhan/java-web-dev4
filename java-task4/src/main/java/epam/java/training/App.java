package epam.java.training;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Epam - Java Web Development Training
 * Assel Sarzhanova
 * Task 4
 */

public class App {
    public static void main( String[] args ) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int matrixSize;
        while(true) {
            try {
                System.out.println("Enter Matrix Size:");
                matrixSize = Integer.parseInt(bufferedReader.readLine());
                if (matrixSize <= 0)
                    throw new NumberFormatException("Negative or Zero Matrix size. Size = " + matrixSize);
                break;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        int[][] matrix = new int[matrixSize][matrixSize];
        fillMatrixWithRandomNumbers(matrix);
        printMatrix(matrix);
        
        int columnNumber = new Random().nextInt(matrixSize);

        orderMatrixRowsByIncreasingElementsOfColumn(matrix, columnNumber);
        System.out.println("Randomly generated Column Number = " + (columnNumber + 1));
        System.out.printf("Matrix rows follow [%d-th Columns] increasing order \n\n", columnNumber + 1);
        printMatrix(matrix);

        int rowNumber = new Random().nextInt(matrixSize);
        orderMatrixColumnsByIncreasingElementsOfRow(matrix, rowNumber);
        System.out.println("Randomly generated Row Number = " + (rowNumber + 1));
        System.out.printf("Matrix columns follow [%d-th Rows] increasing order \n\n", rowNumber + 1);
        printMatrix(matrix);

        printSumOfElementsBetweenFirstAndSecondPositive(matrix);

        int matrixMaximumElement = getMatrixMaximumElement(matrix);
        System.out.println("Maximum Element of Matrix = " + matrixMaximumElement);
        Set<Integer> removableRowIndexesSet = getMatrixRowIndexesToDelete(matrix, matrixMaximumElement);
        Set<Integer> removableColumnIndexesSet = getMatrixColumnIndexesToDelete(matrix, matrixMaximumElement);
        matrix = getMatrixWithDeletedRowsColumns(matrix, removableRowIndexesSet, removableColumnIndexesSet);
        printMatrix(matrix);

        bufferedReader.close();
    }

    public static void fillMatrixWithRandomNumbers(int[][] matrix){
        int maximumElementValue = 100;
        int minimumElementValue = -100;
        Random random = new Random();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = random.nextInt(maximumElementValue - minimumElementValue) + minimumElementValue;
            }
        }
    }

    public static void orderMatrixRowsByIncreasingElementsOfColumn(int[][] matrix, int columnNumber){
        for (int k = 0; k < matrix.length - 1; k++) {
            for (int i = 0; i < matrix.length - 1; i++) {
                if(matrix[i][columnNumber] > matrix[i+1][columnNumber]){
                    for (int j = 0; j < matrix.length; j++) {
                        int valueHolder = matrix[i][j];
                        matrix[i][j] = matrix[i+1][j];
                        matrix[i+1][j] = valueHolder;
                    }
                }
            }
        }
    }

    public static void orderMatrixColumnsByIncreasingElementsOfRow(int[][] matrix, int rowNumber){
        for (int k = 0; k < matrix.length - 1; k++) {
            for (int j = 0; j < matrix.length - 1; j++) {
                if(matrix[rowNumber][j] > matrix[rowNumber][j+1]){
                    for (int i = 0; i < matrix.length; i++) {
                        int valueHolder = matrix[i][j];
                        matrix[i][j] = matrix[i][j+1];
                        matrix[i][j+1] = valueHolder;
                    }
                }
            }
        }
    }

    public static void printSumOfElementsBetweenFirstAndSecondPositive(int[][] matrix){
        List<String> sumResultList = new ArrayList<>();
        System.out.println("------------------------------------------------");
        for (int i = 0; i < matrix.length; i++) {
            sumResultList = getElementsSumResultForEachRow(matrix[i]);
            System.out.printf("%d. Elements: %s, %s; Sum: %s;\n", (i + 1), sumResultList.get(0), sumResultList.get(1), sumResultList.get(2));
        }
        System.out.println("------------------------------------------------");
    }

    public static List<String> getElementsSumResultForEachRow(int[] rowOfMatrixArray){
        List<String> firstSecondSumList = new ArrayList<>();
        int first;
        int second;
        int sum = 0;
        for (first = 0; first < rowOfMatrixArray.length; first++) {
            if(rowOfMatrixArray[first] > 0) break;
        }
        for (second = first+1; second < rowOfMatrixArray.length; second++){
            if(rowOfMatrixArray[second] > 0) break;
        }
        if((first + 1) < second && second != rowOfMatrixArray.length){
            for (int i = first + 1; i < second; i++) {
                sum += rowOfMatrixArray[i];
            }
            firstSecondSumList.add(Integer.toString(rowOfMatrixArray[first]));
            firstSecondSumList.add(Integer.toString(rowOfMatrixArray[second]));
            firstSecondSumList.add(Integer.toString(sum));

        }else{
            if(first < rowOfMatrixArray.length)
                firstSecondSumList.add(Integer.toString(rowOfMatrixArray[first]));
            else
                firstSecondSumList.add("not found");

            if(second < rowOfMatrixArray.length)
                firstSecondSumList.add(Integer.toString(rowOfMatrixArray[second]));
            else
                firstSecondSumList.add("not found");

            firstSecondSumList.add("No Elements Found to sum");
        }
        return firstSecondSumList;
    }

    public static int getMatrixMaximumElement(int[][] matrix){
        int maximumValue = (matrix.length > 0) ? matrix[0][0]: 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if(matrix[i][j] > maximumValue)
                    maximumValue = matrix[i][j];
            }
        }
        return maximumValue;
    }

    public static Set<Integer> getMatrixRowIndexesToDelete(int[][] matrix, int maximumElement){
        Set<Integer> rowIndexSet = new HashSet<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if(matrix[i][j] == maximumElement)
                    rowIndexSet.add(i);
            }
        }

        return rowIndexSet;
    }

    public static Set<Integer> getMatrixColumnIndexesToDelete(int[][] matrix, int maximumElement){
        Set<Integer> columnIndexSet = new HashSet<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if(matrix[i][j] == maximumElement)
                    columnIndexSet.add(j);
            }
        }
        return columnIndexSet;
    }

    public static int[][] getMatrixWithDeletedRowsColumns(int[][] matrix, Set<Integer> rowIndexSet, Set<Integer> columnIndexSet){
        int[][] newMatrix = new int[matrix.length - rowIndexSet.size()][matrix.length - columnIndexSet.size()];

        for (int i = 0, l = 0; i < matrix.length && l < newMatrix.length; i++, l++) {
            if(rowIndexSet.contains(i)){
                l--;
                continue;
            }
            for (int j = 0, m = 0; j < matrix[i].length && m < newMatrix[l].length; j++, m++) {
                if(columnIndexSet.contains(j)){
                    m--;
                    continue;
                }
                newMatrix[l][m] = matrix[i][j];
            }
        }

        return newMatrix;
    }

    public static void printMatrix(int[][] matrix){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}
